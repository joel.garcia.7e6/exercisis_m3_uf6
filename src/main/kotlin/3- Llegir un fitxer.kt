import java.io.File
import java.io.IOException

val file = File("Ex3.txt")
fun main(){
    try {
        val reader = file.readText()
        println(reader)
    }catch (e: IOException){
        println("S'ha produït un error d'entrada/sortida:")
    }
}