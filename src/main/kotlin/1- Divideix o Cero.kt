import java.util.Scanner

val sc=Scanner(System.`in`)
fun main(){
    println("Indica un divident i un divisor")
    val num1 = sc.nextInt()
    val num2 = sc.nextInt()
    var resultat = dividirCero(num1,num2)
    println(resultat)
}
fun dividirCero(divident:Int, divisor:Int):Int{
    var resultat  = 0
    try {
        resultat = divident/divisor
    }catch (e: ArithmeticException){
        println("Error: Divisió per zero")
    }
    return resultat

}