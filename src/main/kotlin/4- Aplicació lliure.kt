import java.lang.NumberFormatException

fun main() {
    val lista: MutableList<Int> = mutableListOf(1, 2, 3)
    try {
        lista.add(4)
        val item = lista[2]
        println("El valor de l'element és: $item")
    } catch (e: NoSuchElementException) {
        println("S'ha produït una excepció de tipus NoSuchElementException.")
    }
    try {
        lista.removeAt(0)
        println("Aixi ha quedat la llista:\n" +
                "$lista")
        lista.remove(3)
        println("Aixi ha quedat la llista:\n" +
                "$lista")
    } catch (e: UnsupportedOperationException) {
        println("S'ha produït una excepció de tipus UnsupportedOperationException.")
    }
}
